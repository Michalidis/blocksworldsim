domain=$1
problem=$2
agent=pddl4j/build/libs/pddl4j-3.5.0.jar

if [ ! -f $agent ]; then
    echo "Jar file not found!"
    echo `pwd`
fi
java -javaagent:$agent -server -Xms2048m -Xmx2048m fr.uga.pddl4j.planners.hsp.HSP -o $domain -f $problem
