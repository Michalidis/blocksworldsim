### What is this repository for? ###

* Provides a simulation of each state using a planner from [pddl4j project](https://github.com/pellierd/pddl4j) 
* Each step provided by mentioned planner can be obtained as String output in pddl form

### Compatible Platforms ###
* Windows
* Linux

### Requirements ###
* Git
* Gradle
* Java 8

### How do I get set up? ###
## All of these steps must be done before being able to run the simulation for the first time! ##

### Linux ###

* Download this repository 
```
git clone https://Michalidis@bitbucket.org/Michalidis/blocksworldsim.git
```

* Go to Scripts directory
```
cd /path/to/your/repository/clone/Scripts
```

* Execute jar_build.sh script to build pddl4j library
```
./jar_build.sh
```

### Windows ###

* Download the repository using git - same as first step for linux

* Open command line and navigate to the pddl4j subdirectory of this repository

* Execute a Gradlew command to build the pddl4j library dependency
```
gradlew clean install
```

### Finals steps for both Linux and Windows ###

* Go to Constants class and edit the PATH variables to match the paths of your system
* Now you should be able to run the simulation by executing the main method from your favorite IDE or from console.

* To ignore DEBUG output, simply change the static variable DEBUG in Constants Class

### Configuration Information

* The most important constants to configure in Constants.java are PATH_DOMAIN and PATH_PROBLEM

* PATH_DOMAIN specifies a path to the domain that should be used for simulation

* PATH_PROBLEM specifies a path to the problem that should be used for the simulation

* These two PATH constants define behaviour of the entire simulation. 

### Server-Client Communication

* The port the server uses to communicate with the client is specified in Constants.java file

* Once the server is running, client can connect to the server

* Once the client is connected, he immediately receives a Domain and Problem specifically in that order, in a form of string

* These two are separated by a String sequences, also specified in Constants.java file

* Once the client receives this data, he receives one final message, which represents the state at which the simulation currently is

* From this point onwards, the client can send String messages which should represent valid actions and the server receives and processes those.

* After the action is processed, the server sends the new state in which the simulation currently is

* Side note: Invalid messages received by the server results in a reply which contains the current state of simulation

### Client VM Options

* Everytime you are running the client, you need to specify VM options

* With the current implementation, use following VM options
```
-javaagent:<PathToProject>/pddl4j/build/libs/pddl4j-3.5.0.jar -server -Xms2048m -Xmx2048m
```