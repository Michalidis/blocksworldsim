(define (problem BLOCKS-4-0)
(:domain BLOCKS)
(:objects D B A C - block)
(:INIT (CLEAR C) (CLEAR B) (CLEAR D) (ONTABLE C) (ONTABLE B) (ONTABLE D) (HOLDING A))
(:goal (AND (ON D C) (ON C B) (ON B A)))
)
