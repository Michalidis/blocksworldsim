package fr.uga.pddl4j.planners;

import fr.uga.pddl4j.encoding.CodedProblem;

import java.io.IOException;

public interface StringPlanner {
    /**
     * Search a plan for the specified planning problem.
     *
     * @param problem the problem to be solved. The problem cannot be null.
     * @return the solution plan as String or null is no solution was found.
     */
    String searchAsString(final CodedProblem problem) throws IOException;
}
