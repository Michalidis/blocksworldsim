package ExternalPlanners;

import constants.Constants;
import fr.uga.pddl4j.encoding.CodedProblem;
import fr.uga.pddl4j.planners.AbstractPlanner;
import fr.uga.pddl4j.util.Plan;
import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecutor;
import org.apache.commons.exec.ExecuteException;
import org.apache.commons.exec.Executor;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

public class LamaPlannerDockerExecutor extends AbstractPlanner {

    @Override
    public Plan search(CodedProblem problem) {
        return null;
    }

    @Override
    public String searchAsString(CodedProblem problem) throws IOException {
        File agentWorkingDir = new File(".");
        File lamaWorkingDir = new File(agentWorkingDir, Constants.ExternalPlanner.AGENT_WORKING_DIR);
        File resultFile = new File(lamaWorkingDir, "plan.sol");
        System.out.println(lamaWorkingDir.getAbsolutePath());

        Map<String, String> config = new HashMap<>();
        config.put("workingDir", lamaWorkingDir.getCanonicalPath());
        config.put("domain", Constants.ExternalPlanner.PATH_DOMAIN);
        config.put("problem", Constants.ExternalPlanner.PATH_PROBLEM);
        config.put("result", Constants.ExternalPlanner.PATH_SOLUTION);

        CommandLine commandLine = new CommandLine("docker");
        commandLine.addArgument("run");
        commandLine.addArgument("-v");
        commandLine.addArgument("${workingDir}:/data");
        commandLine.addArgument("--rm");
        commandLine.addArgument("lama");
        commandLine.addArgument("${domain}");
        commandLine.addArgument("${problem}");
        commandLine.addArgument("plan.sol");
        commandLine.setSubstitutionMap(config);

        final Executor executor = new DefaultExecutor();
        executor.setWorkingDirectory(lamaWorkingDir);
        executor.setExitValue(0);

        // SYNC EXECUTION
        try {
            executor.execute(commandLine);
        } catch (ExecuteException e) {
            System.out.println("Exec Failure");
            return null;
        }

        // NOW RESULT FILE SHOULD BE READY
        if (!resultFile.exists()) {
            System.out.println("plan not found");
            return null;
        }

        // PROCESS RESULT
        String resultLines = String.join("\n\r", Files.readAllLines(Paths.get(resultFile.getPath())));

        // add step numbers, lama does not include them
        String[] lines = resultLines.split("[\n\r]+");
        StringBuilder resLines = new StringBuilder();

        for (int i = 0; i < lines.length; i++) {
            if (!lines[i].contains(";"))
                resLines.append(i).append(" : ").append(lines[i]).append("\n");
        }

        resultLines = resLines.toString();

        //rename both moves to "move"
        resultLines = resultLines.replaceAll("move[12]", "move");

//        System.out.println("PLAN");
//        System.out.println(resultLines);

        resultFile.delete();

        return resultLines;
    }
}
