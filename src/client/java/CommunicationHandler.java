import com.sun.deploy.util.StringUtils;
import com.sun.prism.shader.AlphaOne_LinearGradient_AlphaTest_Loader;
import constants.Constants;

import java.io.*;
import java.net.SocketException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static constants.Constants.ExternalPlanner.IGNORE_EVENTS;
import static constants.Constants.Networking.*;

class CommunicationHandler {
    private Thread thread;
    private Agent agent;
    private boolean EnvChanged;

    void start() {
        thread.start();
    }

    void interrupt() {
        thread.interrupt();
    }

    CommunicationHandler(BufferedReader input, PrintWriter output, Agent agent) {
        EnvChanged = false;
        this.agent = agent;
        this.thread = new Thread(() -> {
            try {
                HandleSocketOutput(input, output);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    private void HandleSocketOutput(BufferedReader input, PrintWriter output) throws IOException {
        String inputLn;
        try {
            while (!Objects.equals(inputLn = input.readLine(), MSG_DEAD_END)) {
                switch (inputLn) {
                    case FILE_DOMAIN_NAME:
                        LoadFile(input, true);
                        break;
                    case FILE_PROBLEM_NAME:
                        LoadFile(input, false);
                        break;
                    case MSG_EVENT_APPLIED:
                        if (Constants.DEBUG)
                            System.out.println("Environment has changed!");
                        EnvChanged = true;
                        break;
                    case CUR_STATE:
                        if (EnvChanged) {
                            ModifyProblem(input);
                            if (Constants.DEBUG)
                                System.out.println("Replanning . . .");
                            agent.GetPlan();
                        }
                        if (agent.SendNextStep(output) && Constants.DEBUG)
                            EnvChanged = false;
                        break;
                    default:
                        if (Constants.DEBUG) {
                            System.out.println("This line was not processed:");
                            System.out.println("\t" + inputLn);
                            System.out.println("Continuing according to old plan.");
                        }
                        break;
                }
            }
        } catch (NullPointerException | SocketException npe) {
            System.out.println("Goal has been reached! \nEnd of simulation . . .");
            System.exit(0);
        }
    }

    private void ModifyProblem(BufferedReader input) throws IOException {
        String inputLn = input.readLine();
        if (Constants.DEBUG) {
            System.out.println("Modifying Problem File . . .");
            System.out.println(inputLn);
        }

        File problemFile = agent.getProblem();

        StringBuilder fileContent = new StringBuilder();
        try (BufferedReader br = new BufferedReader(new FileReader(problemFile))) {

            if (Constants.DEBUG)
                System.out.println("Reading old content of problem file. . .");

            String line;
            while ((line = br.readLine()) != null) {
                fileContent.append(line).append(System.lineSeparator());
            }

        }
        String newContent = fileContent.toString().replaceAll("((?<=\\(:)INIT(.*)(?=\\)))", "INIT" + inputLn);

        try (PrintWriter pw = new PrintWriter(problemFile)) {
            pw.write(newContent);
            if (Constants.DEBUG)
                System.out.println("Old content of problem file has been modified. . .");
        }
    }

    private void LoadFile(BufferedReader input, boolean isDomain) throws IOException {
        File f;
        if (isDomain) {
            f = new File(Constants.ExternalPlanner.AGENT_WORKING_DIR + Constants.ExternalPlanner.PATH_DOMAIN);
        } else {
            f = new File(Constants.ExternalPlanner.AGENT_WORKING_DIR + Constants.ExternalPlanner.PATH_PROBLEM);
        }

        String inputLn;
        List<String> lines = new ArrayList<>();

        while (!Objects.equals(inputLn = input.readLine(), FILE_EOF_SEQ)) {
            lines.add(inputLn);
        }


        if (IGNORE_EVENTS && isDomain) {
            List<String> _lines = new ArrayList<>();
            int c = 0;
            for (String _line : lines) {
                if (_line.contains("(:event") || c != 0) {
                    int len = _line.length();
                    c += len - _line.replaceAll("\\(", "  ").length() - (len - _line.replaceAll("\\)", "  ").length());
                } else {
                    _lines.add(_line);
                }
            }
            while (c < 0) {
                _lines.add(")");
                c++;
            }
            lines = _lines;
        }

        if (f.delete())
            System.out.println("Old File '" + f.getName() + "' Has Been Removed.");
        else {
            System.out.println("Old File '" + f.getName() + "' Could not be Removed.");
        }

        if (f.createNewFile()) {
            Files.write(f.toPath(), lines);
            System.out.println("File '" + f.getName() + "' Loaded Successfully.");
        } else
            System.out.println("Failed to create a new file with name '" + f.getName() + "'.");

        if (isDomain)
            agent.setDomain(f);
        else
            agent.setProblem(f);
    }
}
