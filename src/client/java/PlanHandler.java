import constants.Constants;
import fr.uga.pddl4j.encoding.CodedProblem;
import fr.uga.pddl4j.util.BitOp;
import fr.uga.pddl4j.util.Plan;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

class PlanHandler {
    private List<String> preparedPlan;
    private int currentPosition;

    void setPreparedPlan(String plan) {
        currentPosition = 0;
        preparedPlan = new ArrayList<>();

        Pattern pattern = Pattern.compile("\\(.*\\)");
        Matcher matcher = pattern.matcher(plan);
        while (matcher.find())
            preparedPlan.add(matcher.group(0).substring(1, matcher.group(0).length() - 1));
    }

    private String getNextStepInPlan() {
        if (currentPosition < preparedPlan.size())
            return preparedPlan.get(currentPosition++);
        return null;
    }

    String getNextStepInPlan_str() throws IOException {
//        BitOp planStep = getNextStepInPlan();
//        BitOp planStep = null;
//        if (planStep == null) {
//            if (Constants.DEBUG) {
//                System.out.println("No more steps in current plan!");
//            }
//            return null;
//        }
//
//        if (codedProblem == null) {
//            if (Constants.DEBUG) {
//                System.out.println("Coded Problem is missing. ERROR!");
//            }
//            return null;
//        }
//
//        return codedProblem.toShortString(planStep);
        return getNextStepInPlan();
    }
}
