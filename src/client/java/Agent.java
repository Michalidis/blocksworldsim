import constants.Constants;
import fr.uga.pddl4j.encoding.CodedProblem;
import fr.uga.pddl4j.parser.ErrorManager;
import fr.uga.pddl4j.planners.AbstractPlanner;
import fr.uga.pddl4j.planners.ProblemFactory;
import fr.uga.pddl4j.util.BitOp;
import fr.uga.pddl4j.util.Plan;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

class Agent {

    private CommunicationHandler sr;

    private File domain;
    private File problem;

    private ProblemFactory factory;
    private AbstractPlanner planner;
    private PlanHandler planHandler;
    private CodedProblem codedProblem;

    Agent(BufferedReader input, PrintWriter output, AbstractPlanner planner) {
        domain = null;
        problem = null;

        factory = new ProblemFactory();
        this.planner = planner;
        planHandler = new PlanHandler();
        codedProblem = null;

        sr = new CommunicationHandler(input, output, this);
    }

    File getProblem() {
        return problem;
    }

    CodedProblem getCodedProblem() {
        return codedProblem;
    }

    void setDomain(File domain) {
        this.domain = domain;
        GetPlan();
    }

    void setProblem(File problem) {
        this.problem = problem;
        GetPlan();
    }

    void start() {
        sr.start();
    }

    void GetPlan() {
        if (domain != null && problem != null) {
            try {
                ErrorManager errorManager = factory.parse(domain, problem);
                if (!errorManager.isEmpty()) {
                    errorManager.printAll();
                    System.exit(0);
                }

                // Encode and simplify the planning problem in a compact representation
                codedProblem = factory.encode();
                if (!codedProblem.isSolvable()) {
                    System.out.println("goal can be simplified to FALSE. "
                            + "no search will solve it");
                    System.exit(0);
                }

                // Search for a solution plan
                String preparedPlan = planner.searchAsString(codedProblem);

                planHandler.setPreparedPlan(preparedPlan);
                if (preparedPlan != null) {
                    if (Constants.DEBUG) {
                        System.out.println("Found following plan:");
                        System.out.println(preparedPlan);
                    }
                } else {
                    if (Constants.DEBUG)
                        System.out.println("No plan found . . .");
                }
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }
        }
    }

    boolean SendNextStep(PrintWriter output) throws IOException {
        String nextStep = planHandler.getNextStepInPlan_str();

        if (nextStep != null) {
            if (Constants.DEBUG) {
                System.out.println("Sending operation: " + nextStep);
            }
            output.println(nextStep);
            return true;
        } else {
            return false;
        }
    }
}
