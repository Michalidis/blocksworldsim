import java.io.*;
import java.net.Socket;

import static constants.Constants.ExternalPlanner.PLANNER;
import static constants.Constants.Networking.SOCKET_PORT;


public class Client {

    public static void main(String[] args) throws IOException {
        Socket s = new Socket("127.0.0.1", SOCKET_PORT);
        BufferedReader input = new BufferedReader(new InputStreamReader(s.getInputStream()));
        PrintWriter out = new PrintWriter(s.getOutputStream(), true);
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));

        Agent agent = new Agent(input, out, PLANNER);
        agent.start();
    }
}
