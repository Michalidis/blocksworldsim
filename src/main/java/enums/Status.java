package enums;

public enum Status {
    CLEAR,
    ONTABLE,
    HOLDING,
    ON,
    HANDEMPTY;

    @Override
    public String toString() {
        return name();
    }
}
