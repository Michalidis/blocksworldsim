package enums;

public enum Action {
    PICK_UP,
    STACK,
    UNSTACK,
    PUT_DOWN;

    @Override
    public String toString() {
        return name().toLowerCase().replaceAll("_", "-");
    }
}
