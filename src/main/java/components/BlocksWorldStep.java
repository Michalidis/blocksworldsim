package components;

import enums.Action;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

public class BlocksWorldStep {

    private String[] target_object_names = null;
    private Action action = null;

    public BlocksWorldStep(String _target_object_name, Action _action) {
        target_object_names = _target_object_name.split("\\s+");
        action = _action;
    }

    /**
     * Initializes an instance of BWStep from String.
     *
     * @param step A valid representation of BlocksWorldStep.
     */
    BlocksWorldStep(String step) {
        String[] step_split = step.split("\\s+");

        action = Action.valueOf(step_split[0].replaceAll("-", "_").toUpperCase());
        target_object_names = step_split.length == 2 ? new String[]{step_split[1]} : new String[]{step_split[1], step_split[2]};
    }

    @Override
    public String toString() {
        if (target_object_names.length == 1)
            return action + " " + target_object_names[0];
        else if (target_object_names.length == 2)
            return action + " " + target_object_names[0] + " " + target_object_names[1];
        else
            throw new NotImplementedException();
    }

    Action getAction() {
        return action;
    }

    String getTarget_object_name(int index) {
        if (index < target_object_names.length)
            return target_object_names[index].toUpperCase();
        else
            return null;
    }
}
