package components;

import enums.Status;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static enums.Action.*;
import static enums.Status.*;

public class BlocksWorldState {
    private List<BlocksWorldObject> state = null;
    private Status hand_status = null;

    public BlocksWorldState(BlocksWorldObject... objects) {
        state = new ArrayList<>();
        state.addAll(Arrays.asList(objects));
    }

    /**
     * Initialize a BlocksWorldState from a String.
     *
     * @param _state Valid String representation of BWState.
     */
    public BlocksWorldState(String _state) {
        state = new ArrayList<>();

        Pattern pattern = Pattern.compile("((?<=\\()([A-Z]*\\s*[A-Z]*)*(?=\\)))");
        Matcher matcher = pattern.matcher(_state);

        while (matcher.find()) {
            String[] group_split = matcher.group(1).split("\\s+");

            Status group_status = Status.valueOf(group_split[0]);
            String name_one = group_split.length < 2 ? null : group_split[1];
            String name_two = group_split.length < 3 ? null : group_split[2];

            switch (group_status) {
                case HANDEMPTY:
                    hand_status = HANDEMPTY;
                    break;
                case CLEAR:
                    PrepareBWObject(name_one, group_status);
                    break;
                case ONTABLE:
                    PrepareBWObject(name_one, group_status);
                    break;
                case HOLDING:
                    PrepareBWObject(name_one, group_status);
                    break;
                case ON:
                    PrepareBWObject(name_one, name_two, group_status);
                    break;
                default:
                    throw new NotImplementedException();
            }
        }
    }

    /**
     * Prepares a BWObject for list of objects. Existing object will be expanded, non-existing object will be created.
     *
     * @param obj_name   Name of affected object.
     * @param obj_status New status of affected object.
     */
    private void PrepareBWObject(String obj_name, Status obj_status) {
        BlocksWorldObject BWObj;
        if ((BWObj = getObject(obj_name)) == null)
            state.add(new BlocksWorldObject(obj_name, obj_status));
        else
            BWObj.AddStatus(obj_status);
    }

    /**
     * Prepares a BWObject for list of objects. Existing object will be expanded, non-existing object will be created.
     *
     * @param obj_name        Name of affected object.
     * @param linked_obj_name New status of affected object.
     * @param obj_status      Name of linked object.
     */
    private void PrepareBWObject(String obj_name, String linked_obj_name, Status obj_status) {
        BlocksWorldObject BWObj;
        if ((BWObj = getObject(obj_name)) == null)
            state.add(new BlocksWorldObject(obj_name, linked_obj_name, obj_status));
        else {
            BWObj.AddStatus(obj_status);
            BWObj.SetLinkedObjectName(linked_obj_name);
        }
    }

    /**
     * Prepares a String of all objects with their status.
     *
     * @return All objects with their status in one String.
     */
    public String toString() {
        StringBuilder sb = new StringBuilder();
        state.forEach(x -> sb.append(x.toString()).append(" "));

        if (hand_status != null)
            sb.append("(").append(hand_status).append(")").append(" ");

        return sb.substring(0, sb.length() - 1);
    }

    /**
     * Finds and returns a BlocksWorldObject based on the name.
     *
     * @param name Name of object we are searching for.
     * @return Entire BlocksWorldObject.
     */
    private BlocksWorldObject getObject(String name) {
        return state.stream().filter(x -> x.getName().equals(name)).findFirst().orElse(null);
    }

    /**
     * Prepares a String of all object names.
     *
     * @return Names of objects in form similar to :objects in problem file.
     */
    public String toObjects() {
        StringBuilder sb = new StringBuilder();
        state.forEach(x -> sb.append(x.getName()).append(" "));
        return sb.substring(0, sb.length() - 1);
    }

    /**
     * Performs a step of simulation on the corresponding object(s).
     *
     * @param step Step to be executed.
     * @return State in which the simulation is after executing the step.
     */
    public BlocksWorldState PerformStep(BlocksWorldStep step) {
        AffectHandStatus(step);


        BlocksWorldObject object1 = getObject(step.getTarget_object_name(0));
        BlocksWorldObject object2 = getObject(step.getTarget_object_name(1));

        if (step.getAction() == STACK)
            object1.SetLinkedObjectName(object2.getName());
        else if (step.getAction() == UNSTACK)
            object1.SetLinkedObjectName(null);

        object1.PerformStep(step);
        if (object2 != null)
            object2.PerformStep(step, true);


        return this;
    }

    private void AffectHandStatus(BlocksWorldStep step) {
        switch (step.getAction()) {
            case STACK:
                hand_status = HANDEMPTY;
                break;
            case PUT_DOWN:
                hand_status = HANDEMPTY;
                break;
            case PICK_UP:
                hand_status = null;
                break;
            case UNSTACK:
                hand_status = null;
                break;
            default:
                break;
        }
    }
}
