package components;

import enums.Status;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static enums.Status.*;

public class BlocksWorldObject {
    private String name = null;
    private List<Status> status = null;
    private String linked_object_name = null;


    BlocksWorldObject(String _name, String _linked_object_name, Status... statuses) {
        name = _name;
        status = new ArrayList<>();
        status.addAll(Arrays.asList(statuses));
        linked_object_name = _linked_object_name;
    }

    BlocksWorldObject(String _name, Status... statuses) {
        name = _name;
        status = new ArrayList<>();
        if (statuses[0] != null)
            status.addAll(Arrays.asList(statuses));
    }


    public String toString() {
        StringBuilder sb = new StringBuilder();
        status.forEach(x -> {
            if (x == ON)
                sb.append("(").append(x).append(" ").append(name).append(" ").append(linked_object_name).append(")").append(" ");
            else
                sb.append("(").append(x).append(" ").append(name).append(")").append(" ");
        });
        return sb.substring(0, sb.toString().length() - 1);
    }

    String getName() {
        return name;
    }

    /**
     * Adds a new status to list of active states of object.
     *
     * @param _status A status to add to object.
     */
    void AddStatus(Status _status) {
        status.add(_status);
    }

    /**
     * Adds a Linked object name to instance of this object.
     *
     * @param _name Name of the linked object - does not have to be initialized at time of linking.
     */
    void SetLinkedObjectName(String _name) {
        linked_object_name = _name;
    }

    /**
     * Performs a step and affects the object directly.
     * Acts as help method to simulate default method parameter value.
     *
     * @param step             Step to be executed on this object.
     * @param as_linked_object If true, handles the object as linked_object. If False, it is handled normally.
     */
    void PerformStep(BlocksWorldStep step, boolean as_linked_object) {
        if (!as_linked_object) {
            PerformStep(step);
        } else {
            switch (step.getAction()) {
                case STACK:
                    status.remove(CLEAR);
                    break;
                case UNSTACK:
                    status.add(CLEAR);
                    break;
                default:
                    throw new NotImplementedException();
            }
        }
    }

    /**
     * Performs a step and affects the object directly.
     *
     * @param step Step to be executed on this object.
     */
    void PerformStep(BlocksWorldStep step) {
        switch (step.getAction()) {
            case STACK:
                status.remove(HOLDING);
                status.add(ONTABLE);
                status.add(CLEAR);
                status.add(ON);
                break;
            case UNSTACK:
                status.remove(ON);
                status.remove(ONTABLE);
                status.remove(CLEAR);
                status.add(HOLDING);
                break;
            case PICK_UP:
                status.remove(ONTABLE);
                status.remove(CLEAR);
                status.add(HOLDING);
                break;
            case PUT_DOWN:
                status.remove(HOLDING);
                status.add(CLEAR);
                status.add(ONTABLE);
                break;
            default:
                throw new NotImplementedException();
        }
    }
}
