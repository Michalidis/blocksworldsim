package components;

public class StepStateTuple {
    private String _step = null;
    private String _state = null;

    private BlocksWorldStep step = null;
    private BlocksWorldState state = null;

    public StepStateTuple(String _string, boolean is_state) {
        if (is_state)
            _state = _string;
        else
            _step = _string;
    }

    public StepStateTuple(String __state, String __step) {
        _state = __state;
        _step = __step;
    }

    public BlocksWorldStep getStep() {
        if (step == null && _step != null)
            step = new BlocksWorldStep(_step);
        return step;
    }

    public void SetStepIfNull(String __step) {
        if (_state == null)
            _step = __step;
    }

    public void SetStateIfNull(String __state) {
        if (_state == null)
            _state = __state;
    }

    /**
     * Retrieves an instance of BlocksWorldState obtained from its String representation.
     *
     * @return A valid instance of BlocksWorldState.
     */
    public BlocksWorldState getState() {
        if (state == null)
            state = new BlocksWorldState(_state);
        return state;
    }

}
