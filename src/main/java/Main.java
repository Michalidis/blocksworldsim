import constants.Constants;
import managers.StateManager;
import fr.uga.pddl4j.encoding.CodedProblem;
import fr.uga.pddl4j.parser.ErrorManager;
import fr.uga.pddl4j.planners.ProblemFactory;
import simulators.HspSimulation;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.file.Files;

import static constants.Constants.PATH_DOMAIN;
import static constants.Constants.PATH_PROBLEM;

public class Main {
    public static void main(String[] args) throws Exception {
//        UseStateManager();
        UsePddl4jParser();
    }


    private static void UseStateManager() throws Exception {
        StateManager sm = new StateManager();
        sm.SetUpStateManager(PATH_DOMAIN, PATH_PROBLEM);
        sm.ExecutePlan();
    }

    private static void RunSimulationOverNetwork(CodedProblem pb, File domain, File problem) throws IOException {
        try (ServerSocket listener = new ServerSocket(Constants.Networking.SOCKET_PORT)) {
            while (true) {
                try (Socket socket = listener.accept()) {
                    PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
                    OutputFileToClient(domain, out, true);
                    OutputFileToClient(problem, out);
                    final HspSimulation planner = new HspSimulation();
                    planner.simulate(pb, socket);
                } catch (Exception e) {
                    System.out.println("Socket has been closed unexpectedly.");
                }
            }
        }
    }

    private static void OutputFileToClient(File file, PrintWriter pwriter, boolean isDomain) throws IOException {
        if (isDomain)
            pwriter.println(Constants.Networking.FILE_DOMAIN_NAME);
        if (Constants.DEBUG) {
            Files.lines(file.toPath()).forEach(x -> {
                pwriter.println(x);
                System.out.println(x);
            });
            pwriter.println(Constants.Networking.FILE_EOF_SEQ);
            System.out.println(Constants.Networking.FILE_EOF_SEQ);
            return;
        }
        Files.lines(file.toPath()).forEach(pwriter::println);
        pwriter.println(Constants.Networking.FILE_EOF_SEQ);
    }

    private static void OutputFileToClient(File file, PrintWriter ostream) throws IOException {
        ostream.println(Constants.Networking.FILE_PROBLEM_NAME);
        OutputFileToClient(file, ostream, false);
    }

    private static void UsePddl4jParser() throws IOException {
        File domain = new File(PATH_DOMAIN);
        File problem = new File(PATH_PROBLEM);

        // Create the problem factory
        final ProblemFactory factory = new ProblemFactory();

        // Parse the domain and the problem
        ErrorManager errorManager = factory.parse(domain, problem);
        if (!errorManager.isEmpty()) {
            errorManager.printAll();
            System.exit(0);
        }

        // Encode and simplify the planning problem in a compact representation
        final CodedProblem pb = factory.encode();
        if (!pb.isSolvable()) {
            System.out.println("goal can be simplified to FALSE. "
                    + "no search will solve it");
            System.exit(0);
        }

        RunSimulationOverNetwork(pb, domain, problem);
    }
}
