package managers;

import components.BlocksWorldState;
import components.BlocksWorldStep;
import components.StepStateTuple;
import helpers.FileHelper;
import helpers.ScriptHelper;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import static constants.Constants.DEBUG;
import static constants.Constants.PATH_SCRIPT;

public class StateManager {

    private List<StepStateTuple> states = null;
    private String goal = null;
    private String problem_name = null;
    private String domain_name = null;

    private FileHelper file_helper = null;
    private ScriptHelper script_helper = null;


    /**
     * Sets up/Resets StateManager and makes it ready for performing actions.
     *
     * @param domain_path  Path to file containing domain of problem.
     * @param problem_path Path to file containing problem.
     * @throws Exception Not handled by this method.
     */
    public void SetUpStateManager(String domain_path, String problem_path) throws Exception {
        file_helper = new FileHelper(problem_path);
        script_helper = new ScriptHelper(PATH_SCRIPT, domain_path, problem_path);

        states = new ArrayList<>();
        states.add(new StepStateTuple(file_helper.getState_initial(), true));

        goal = file_helper.getState_final();
        problem_name = file_helper.getProblem_name();
        domain_name = file_helper.getDomain_name();

        if (DEBUG) {
            System.out.println("Problem loaded successfully . . .");
            System.out.println("\tStarting State (Init):\n\t\t" + states.get(0).getState());
            System.out.println("\tFinal State (Goal):\n\t\t" + goal);
            System.out.println();
        }
    }

    /**
     * Loads and Executes plan.
     */
    public void ExecutePlan() {
        for (String s : script_helper.getParsedPlan()) {
            if (DEBUG)
                System.out.println("Executing action . . . " + s);

            states.add(new StepStateTuple(s, false));

            StepStateTuple prev_tuple = states.get(states.size() - 2);
            StepStateTuple curr_tuple = states.get(states.size() - 1);
            BlocksWorldStep curr_step = curr_tuple.getStep();

            BlocksWorldState prev_state_copy = new BlocksWorldState(prev_tuple.getState().toString());
            curr_tuple.SetStateIfNull(prev_state_copy.PerformStep(curr_step).toString());

            if (DEBUG)
                System.out.println("New state entered . . . " + curr_tuple.getState() + "\n");
        }

        if (DEBUG) {
            System.out.println("Final state reached after execution of all steps . . .\n\t" + states.get(states.size() - 1).getState() + "\n\n");
            System.out.println(this);
            System.out.println("Goal State:\n\t\t\t" + goal);
            System.out.println();
        }
    }

    /**
     * Generates an output from loaded resources to match a problem file.
     *
     * @return A string representation of definition of problem.
     */
    public String generateProblemFileText(int state_index) {

        return "(define (problem " + problem_name + ")\n" +
                "(:domain " + domain_name + ")\n" +
                "(:objects " + file_helper.getAll_objects() + " - block)\n" +
                "(:INIT " + states.get(state_index).getState() + ")\n" +
                "(:goal " + goal + ")\n" +
                ")";
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        final int[] counter = {0};

        sb.append("_____________________________________________________________________________________________\n");
        sb.append("Plan Execution Order:\n");

        states.forEach(x -> {
            if (x.getStep() == null)
                sb.append("\t[")
                        .append(getNumberDecFormat(counter[0]))
                        .append("]:\t")
                        .append(x.getState())
                        .append("\n");
            else {
                counter[0]++;
                sb.append("\t[")
                        .append(getNumberDecFormat(counter[0]))
                        .append("]:\t")
                        .append(x.getStep())
                        .append("\n\t\t\t")
                        .append(x.getState())
                        .append("\n");
            }
        });
        return sb.toString();
    }

    private String getNumberDecFormat(int value) {
        return new DecimalFormat("00").format(value);
    }

}
