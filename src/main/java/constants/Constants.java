package constants;

import ExternalPlanners.LamaPlannerDockerExecutor;
import ExternalPlanners.LamaPlannerNativeExecutor;
import fr.uga.pddl4j.planners.AbstractPlanner;

public class Constants {
    public static final String PATH_SCRIPT = "Scripts/proj_script.sh";
    public static final String PATH_DOMAIN = "pddl4j/pddl/blocksworld/domain2.pddl";
    public static final String PATH_PROBLEM = "pddl4j/pddl/blocksworld/p01.pddl";

    public static final String HEAD_DOMAIN = "domain ";
    public static final String HEAD_OBJECTS = "objects ";
    public static final String HEAD_STATE_INITIAL = "INIT ";
    public static final String HEAD_STATE_FINAL = "goal ";

    public static final String TAIL_OBJECTS = " - block";

    public static final boolean DEBUG = true;
    public static final boolean CHECK_DEAD_END = false;

    public class Networking {
        public static final int SOCKET_PORT = 27072;
        public static final String FILE_DOMAIN_NAME = "-domain:";
        public static final String FILE_PROBLEM_NAME = "-problem:";
        public static final String FILE_EOF_SEQ = "\t&&&&&\t";
        public static final String MSG_DEAD_END = "---Simulation Entered a Dead End State---";
        public static final String MSG_EVENT_APPLIED = "Environment has changed dynamically!";
        public static final String CUR_STATE = "-current state:";
    }

    public static class ExternalPlanner {
        // All paths in this use the path to "fd" folder as prefix //
        public static final boolean IGNORE_EVENTS = true;
        public static final String AGENT_WORKING_DIR = "fd/";
        public static final String PATH_DOMAIN = "client_domain.pddl";
        public static final String PATH_PROBLEM = "client_problem.pddl";
        public static final String PATH_SOLUTION = "plan.sol";

        public static final AbstractPlanner PLANNER = new LamaPlannerNativeExecutor();
    }
}
