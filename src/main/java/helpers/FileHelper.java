package helpers;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import static constants.Constants.*;

public class FileHelper {
    private String file_content = null;

    private String problem_name = null;
    private String domain_name = null;
    private String all_objects = null;
    private String state_initial = null;
    private String state_final = null;

    public String getProblem_name(){
        return problem_name;
    }

    public String getDomain_name() {
        return domain_name;
    }

    public String getAll_objects() {
        return all_objects;
    }

    public String getState_initial(){
        return state_initial;
    }

    public String getState_final() {
        return state_final;
    }

    /**
     * Initializes an instance of this class and immediately reads the content.
     *
     * @param problem_path Path to the file containing problem.
     * @throws Exception A possible exception - not handled.
     */
    public FileHelper(String problem_path) throws Exception {
        loadProblemFile(problem_path);
        parseProblemContent();
    }

    /**
     * Loads content of file at passed path.
     *
     * @param problem_path Path to the file containing problem.
     * @throws IOException A possible exception - not handled.
     */
    private void loadProblemFile(String problem_path) throws IOException {
        Stream<String> problem_lines = Files.lines(Paths.get(problem_path));

        StringBuilder problem_builder = new StringBuilder();
        problem_lines.forEach(x -> problem_builder.append(x).append("\n"));

        file_content = problem_builder.toString();
    }

    /**
     * Parses the loaded file content into variables required by simulation.
     *
     * @throws Exception An unhandled exception.
     */
    private void parseProblemContent() throws Exception {
        Pattern pattern = Pattern.compile("((?<=\\(:)(.*)(?=\\)))");
        Matcher matcher = pattern.matcher(file_content);

        problem_name = get_tail(get_head(file_content, "problem "), ")\n");

        AtomicInteger counter = new AtomicInteger(0);
        AtomicInteger number_of_elems = new AtomicInteger(4);
        while (matcher.find() && counter.get() < number_of_elems.get()) {
            switch (counter.get()) {
                case 0:
                    domain_name = get_head(matcher.group(1), HEAD_DOMAIN);
                    break;
                case 1:
                    all_objects = get_tail(get_head(matcher.group(1), HEAD_OBJECTS), TAIL_OBJECTS);
                    break;
                case 2:
                    state_initial = get_head(matcher.group(1), HEAD_STATE_INITIAL);
                    break;
                case 3:
                    state_final = get_head(matcher.group(1), HEAD_STATE_FINAL);
                    break;
                default:
                    break;
            }
            counter.getAndIncrement();
        }
        if (counter.get() != number_of_elems.get())
            throw new Exception("Invalid amount of matched rows. Matched: " + counter.get() + ", Expected: " + number_of_elems.get() + ".");
    }

    /**
     * Splits string so that it completely cuts off starting substring.
     *
     * @param str      String to be split.
     * @param splitter Substring that determines the index to cut head away.
     * @return Tail of str.
     */
    private String get_head(String str, String splitter) {
        int start_index = str.indexOf(splitter) + splitter.length();
        int end_index = str.length();
        return str.substring(start_index, end_index);
    }

    /**
     * Splits strings so that it complelety cust off ending substring.
     *
     * @param str      String to be split.
     * @param splitter Substring that determines the index to cut tail away.
     * @return Head of str.
     */
    private String get_tail(String str, String splitter) {
        int start_index = 0;
        int end_index = str.indexOf(splitter);
        return str.substring(start_index, end_index);
    }
}
