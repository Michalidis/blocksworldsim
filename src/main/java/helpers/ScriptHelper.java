package helpers;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static constants.Constants.DEBUG;

public class ScriptHelper {
    private boolean isExecuted;

    private String script_name = null;
    private String script_domain = null;
    private String script_problem = null;

    private Runtime runtime = Runtime.getRuntime();

    private String result = null;
    private String result_raw_plan = null;
    private String[] result_plan = null;

    /**
     * Initializes an instance of script helper.
     *
     * @param _script_name    Path to script.
     * @param _script_domain  Path to script domain.
     * @param _script_problem Path to script problem.
     */
    public ScriptHelper(String _script_name, String _script_domain, String _script_problem) {
        script_name = _script_name;
        script_domain = _script_domain;
        script_problem = _script_problem;
        isExecuted = false;
    }

    /**
     * Assigns a new script to helper.
     *
     * @param _script_name    Path to script.
     * @param _script_domain  Path to script domain.
     * @param _script_problem Path to script problem.
     */
    public void AssignNewScript(String _script_name, String _script_domain, String _script_problem) {
        script_name = _script_name;
        script_domain = _script_domain;
        script_problem = _script_problem;
        isExecuted = false;
    }

    /**
     * Parses raw plan from script to a format that is easy to handle.
     *
     * @return An array of actions from plan.
     */
    public String[] getParsedPlan() {
        if (!isExecuted)
            Execute();

        return result_plan;
    }

    /**
     * Gets raw plan.
     *
     * @return Plan retrieved from script without changes.
     */
    public String getPlan() {
        if (!isExecuted)
            Execute();
        return result_raw_plan;
    }

    /**
     * Executes assigned script, prepares the plan and returns entire script output.
     */
    private void Execute() {
        result = null;

        try {

            Process process = runtime.exec(script_name + " " + script_domain + " " + script_problem);
            BufferedReader process_input = new BufferedReader(new InputStreamReader((process.getInputStream())));

            StringBuilder result_builder = new StringBuilder();
            String inputLine;

            while ((inputLine = process_input.readLine()) != null)
                result_builder.append(inputLine).append("\n");

            process_input.close();
            result = result_builder.toString();

        } catch (IOException e) {
            System.out.println(e.getMessage());
        }

        if (DEBUG)
            System.out.println(result);

        result_raw_plan = parseRawPlan();
        parsePlan();
        isExecuted = true;
    }

    /**
     * Retrieves plan obtained by running Execute() method.
     *
     * @return String representation of plan.
     */
    private String parseRawPlan() {
        return result.substring(result.indexOf("\n0") + 1, result.indexOf("]\n\n") + 1);
    }

    private void parsePlan() {
        Pattern pattern = Pattern.compile("(\\(.*\\))");
        Matcher matcher = pattern.matcher(result_raw_plan);

        StringBuilder found_groups = new StringBuilder();
        int group_count = 0;
        while (matcher.find()) {
            found_groups.append(matcher.group(1)).append("\n");
            group_count++;
        }

        Pattern group_pattern = Pattern.compile(("([a-z][a-z -]+)"));
        Matcher group_matcher = group_pattern.matcher(found_groups.toString());

        result_plan = new String[group_count];
        for (int i = 0; i < group_count && group_matcher.find(); i++)
            result_plan[i] = group_matcher.group(1);

        if (DEBUG) {
            System.out.println("Plan received:");
            for (String s : result_plan)
                System.out.println("\t- " + s);
            System.out.println();
        }
    }
}
