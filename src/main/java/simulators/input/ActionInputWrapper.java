package simulators.input;

import java.util.Arrays;

public class ActionInputWrapper {
    private String action_name;
    private String[] action_parameters;


    /**
     * A constructor that allows initialization of all parameters immediately.
     *
     * @param action_name       the name of action.
     * @param action_parameters the parameters of action.
     */
    public ActionInputWrapper(String action_name, String[] action_parameters) {
        this.action_name = action_name;
        this.action_parameters = action_parameters;
    }

    /**
     * Sets the name of action.
     *
     * @param action_name the name of action.
     */
    public void setAction_name(final String action_name) {
        this.action_name = action_name;
    }

    /**
     * Retrieves the name of action.
     *
     * @return the name of action.
     */
    public String getAction_name() {
        return this.action_name;
    }

    /**
     * Sets the action paramters.
     *
     * @param action_parameters the action parameters.
     */
    public void setAction_parameters(final String[] action_parameters) {
        this.action_parameters = action_parameters;
    }

    /**
     * Retrieves the action parameters.
     *
     * @return the action parameters.
     */
    public String[] getAction_parameters() {
        return action_parameters;
    }

    /**
     * Retrieves the string representation in the same format as taken from input.
     *
     * @return a string representation in the same format as taken from input.
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(action_name);

        for (String s : action_parameters) {
            sb.append(" ").append(s);
        }

        return sb.toString();
    }
}
